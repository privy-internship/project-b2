## Task based UI/ Inductive UI

UI yang perlu diperhatikan dengan tips berikut:<br> 
**1. Focus each page on single task**: Pusatkan pada satu task tunggal dalam setiap desain UI.<br>
**2.Make your Tasks be like Commands**: Buat setiap task dalam bentuk perintah
terhadap user.<br>
**3. Be Mindful of Your Model**: Perhatikan model aplikasi
dalam membuat desain UI dan hindari penggunaan CRUD based UI<br> 
**4. Check Your Links**: Buat link yang jelas agar task utama tidak terhambat dengan
link dalam desain UI<br>
**5. Follow Established UI Patterns**: Buatlah desain
UI yang sederhana dan runtut dengan pola untuk memudahkan user dalam
menggunakan desain UI
